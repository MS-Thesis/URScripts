#!/usr/bin/env python
# license removed for brevity
import rospy
from std_msgs.msg import String
import time
import roslib; roslib.load_manifest('ur_modern_driver')
import actionlib
from control_msgs.msg import *
from trajectory_msgs.msg import *
from sensor_msgs.msg import JointState
from math import pi
import numpy as np
q = [1,1,1,0,0,0] 
def talker():
    pub = rospy.Publisher('/ur_driver/URScript', String, queue_size=10)
    rospy.init_node('ur_talker', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    p2= "get_actual_joint_positions()"
    while not rospy.is_shutdown():
        pub_command = "%s \n" % (p2)
        rospy.loginfo(pub_command)
        pub.publish(pub_command)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass